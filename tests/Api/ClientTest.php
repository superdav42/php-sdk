<?php

use Newera\Api\Client;

class ClientTest extends PHPUnit_Framework_TestCase {

	public $client;

	public function setUp() {
		$this->client = new Client(array(
			'environment' => 'testing',
			'username'    => 'testing',
			'secret'      => '$2y$08$tEwCIc9DxDNKRvzHhfRRlOHxDbu.MyIk5EnzEmZVEJ/GeuHXa.tki'
		));
	}

	public function providerSignature() {
		return array(
			array('foo', array(), 'd8ff4ecf0b516923010bb73654b1d150a6dc13dfe1d89d5520bd17a2b351c3e5'),
			array('foo', array('foo' => 1, 'bar' => 2), 'b301653cd5b12f04dacdb57750ea2bfa4ff8058a095a2a50e5a81f448091e5b7'),
			array('ping', array(), 'b4186df0b7e4cecc8fdf6a2babce576674ce9ba25f4a1d1c81da0a2502479b3f'),
			array('orders', array('start' => 0, 'rows' => 20), '16a060c526f2c460815567d73e05a4cdaf48e9ee2e3b9858e9b6933011ad798b'),
			array('orders', array('start' => 25, 'rows' => 60), 'c19e7e7f58f1a46aa19ee5c74a0a1d7e5ca7775c94deba4060744a15288b05be')
		);
	}

	/**
	 * @dataProvider providerSignature
	 */
	public function testSignature($uri, $data, $expected) {
		$actual = $this->client->signRequest($uri, $data);
		$this->assertEquals($expected, $actual);
	}

	public function testPing() {
		$this->assertTrue($this->client->ping());
	}

	public function testGetOrders() {
		//$orders = $this->client->getOrders(1);
		//$this->assertEquals(1, count($orders));
	}

	public function testGetOrderStatus() {
		//$status = $this->client->getOrderStatus(1);
		//$this->assertTrue(!empty($status));
	}
}
