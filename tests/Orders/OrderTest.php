<?php

use Newera\PlatformObjects\Address;
use Newera\PlatformObjects\Customer;
use Newera\PlatformObjects\Item;
use Newera\PlatformObjects\Order;

class OrderTest extends PHPUnit_Framework_TestCase {

	public function providerBasic() {
		return array(
			array(
				array('John Doe', 'Doe Industries', '123 Fake St.', 'Austin', 'TX', '78701', 'US', 'jdoe@gmail.com', '512-123-4567'),
				array('John Doe', 'Doe Industries', '123 Fake St.', 'Austin', 'TX', '78701', 'US'),
				array('John Doe', 'Doe Industries', '123 Fake St.', 'Austin', 'TX', '78701', 'US'),
				array(
					array('abc123',2,'newera','AB100A',NULL),
					array('abc123',1,'external',NULL,'http://foo.com/uploads/bar.jpg')
				)
			)
		);
	}

	/**
	 * @test
	 * @dataProvider providerBasic
	 */
	public function testBasic($customer_data, $shipping_data, $billing_data, $items_data) {
		$order = new Order();

		$order->customer = new Customer(array(
			'name' => $customer_data[0],
			'company' => $customer_data[1],
			'address' => $customer_data[2],
			'city' => $customer_data[3],
			'state' => $customer_data[4],
			'zip' => $customer_data[5],
			'country' => $customer_data[6],
			'email' => $customer_data[7],
			'phone' => $customer_data[8]
		));

		$order->shipping = new Address(array(
			'name' => $shipping_data[0],
			'company' => $shipping_data[1],
			'address' => $shipping_data[2],
			'city' => $shipping_data[3],
			'state' => $shipping_data[4],
			'zip' => $shipping_data[5],
			'country' => $shipping_data[6]
		));

		$order->billing = new Address(array(
			'name' => $billing_data[0],
			'company' => $billing_data[1],
			'address' => $billing_data[2],
			'city' => $billing_data[3],
			'state' => $billing_data[4],
			'zip' => $billing_data[5],
			'country' => $billing_data[6]
		));

		$order->items = array();

		foreach ($items_data as $item_data) {
			$item = new Item($item_data[0], $item_data[1]);
			$item->image->type = $item_data[2];
			$item->image->code = $item_data[3];
			$item->image->url  = $item_data[4];
			$order->items[] = $item;
		}

		$this->assertTrue($order->isValid());
	}

}
