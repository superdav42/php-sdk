<?php

use Newera\PlatformObjects\Customer;

class CustomerTest extends PHPUnit_Framework_TestCase {

	public function providerBasic() {
		return array(
			array('John Doe', 'Doe Industries', '123 Fake St.', 'Austin', 'TX', '78701', 'US', 'jdoe@gmail.com', '512-123-4567'),
			array('Jane Doe', NULL, '456 False Lane', 'Austin', 'TX', '78705', 'US', 'janedoe@yahoo.com', '512-345-6789'),
			array('Jane Doe', '', '456 False Lane', 'Austin', 'TX', '78705', 'US', 'janedoe@yahoo.com', '512-345-6789')
		);
	}

	/**
	 * @test
	 * @dataProvider providerBasic
	 */
	public function testBasic($name, $company, $address1, $city, $state, $zip, $country, $email, $phone) {
		$customer = new Customer(array(
			'name' => $name,
			'company' => $company,
			'address' => $address1,
			'city' => $city,
			'state' => $state,
			'zip' => $zip,
			'country' => $country,
			'email' => $email,
			'phone' => $phone
		));
		$this->assertEquals($name, $customer->name);
		$this->assertEquals($company, $customer->company);
		$this->assertEquals($address1, $customer->address);
		$this->assertEquals($city, $customer->city);
		$this->assertEquals($state, $customer->state);
		$this->assertEquals($zip, $customer->zip);
		$this->assertEquals($country, $customer->country);
		$this->assertEquals($email, $customer->email);
		$this->assertEquals($phone, $customer->phone);
		$this->assertTrue($customer->isValid());
	}

	public function providerInvalid() {
		return array(
			array(array(
				'name' => 'Foo Bar'
			)),
			array(array(
				'city' => 'Austin',
				'state' => 'TX'
			)),
			array(array(
				'name' => 'Foo Bar',
				'city' => 'Austin',
				'state' => 'TX',
				'country' => 'US'
			)),
			array(array(
				'name' => 'Foo Bar',
				'address' => '123 Fake St.',
				'city' => 'Austin',
				'state' => 'TX',
				'country' => 'US'
			)),
			array(array(
				'name' => 'Foo Bar',
				'address' => '123 Fake St.',
				'city' => 'Austin',
				'state' => 'TX',
				'country' => 'US',
				'email' => 'foobar@gmail.com'
			)),
			array(array(
				'name' => 'Foo Bar',
				'address' => '123 Fake St.',
				'city' => 'Austin',
				'state' => 'TX',
				'country' => 'US',
				'phone' => '512-345-6789'
			)),
			array(array(
				'name' => 'Foo Bar',
				'address' => '123 Fake St.',
				'city' => 'Austin',
				'state' => 'TX',
				'country' => 'US',
				'email' => 'foobargmailcom',
				'phone' => '512-345-6789'
			))
		);
	}

	/**
	 * @test
	 * @dataProvider providerInvalid
	 */
	public function testInvalid($customer_data) {
		$customer = new Customer($customer_data);
		$this->assertFalse($customer->isValid());
	}

}
