<?php

use Newera\PlatformObjects\Address;

class AddressTest extends PHPUnit_Framework_TestCase {

	public function providerBasic() {
		return array(
			array('John Doe', 'Doe Industries', '123 Fake St.', 'Austin', 'TX', '78701', 'US'),
			array('Jane Doe', NULL, '456 False Lane', 'Austin', 'TX', '78705', 'US')
		);
	}

	/**
	 * @test
	 * @dataProvider providerBasic
	 */
	public function testBasic($name, $company, $address1, $city, $state, $zip, $country) {
		$address = new Address(array(
			'name' => $name,
			'company' => $company,
			'address' => $address1,
			'city' => $city,
			'state' => $state,
			'zip' => $zip,
			'country' => $country
		));
		$this->assertEquals($name, $address->name);
		$this->assertEquals($company, $address->company);
		$this->assertEquals($address1, $address->address);
		$this->assertEquals($city, $address->city);
		$this->assertEquals($state, $address->state);
		$this->assertEquals($zip, $address->zip);
		$this->assertEquals($country, $address->country);
		$this->assertTrue($address->isValid());
	}

}
