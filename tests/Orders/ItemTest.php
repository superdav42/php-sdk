<?php

use Newera\PlatformObjects\Item;

class ItemTest extends PHPUnit_Framework_TestCase {

	public function providerBasic() {
		return array(
			array('abc123',2,'newera','AB100A',NULL),
			array('abc123',1,'external',NULL,'http://foo.com/uploads/bar.jpg')
		);
	}

	/**
	 * @test
	 * @dataProvider providerBasic
	 */
	public function testBasic($sku, $qty, $type, $code, $url) {
		$item = new Item($sku, $qty);
		$item->image->type = $type;
		$item->image->code = $code;
		$item->image->url  = $url;
		$this->assertEquals($sku,  $item->sku);
		$this->assertEquals($qty,  $item->quantity);
		$this->assertEquals($type, $item->image->type);
		$this->assertEquals($code, $item->image->code);
		$this->assertEquals($url,  $item->image->url);
		$this->assertTrue($item->isValid());
	}

	public function providerInvalid() {
		return array(
			array('',1,'newera','AB100A',NULL),
			array('abc123',NULL,'newera','AB100A',NULL),
			array('abc123',1,'newera','',NULL),
			array('abc123',1,'external',NULL,'')
		);
	}

	/**
	 * @test
	 * @dataProvider providerInvalid
	 */
	public function testInvalid($sku, $qty, $type, $code, $url) {
		$item = new Item($sku, $qty);
		$item->image->type = $type;
		$item->image->code = $code;
		$item->image->url  = $url;
		$this->assertFalse($item->isValid());
	}

	public function providerBorder() {
		return array(
			array('mirror',2,NULL,TRUE),
			array('blur',2,NULL,TRUE),
			array('color',3,'#00FF00',TRUE),
			array('mirror',-1,NULL,FALSE),
			array('blur',-4,NULL,FALSE),
			array('color',2,NULL,FALSE),
			array('foo',2,'blue',FALSE)
		);
	}

	/**
	 * @test
	 * @dataProvider providerBorder
	 */
	public function testBorder($type, $size, $color, $valid)
	{
		$item = new Item('abc123', 1);
		$item->image->code = 'AB100A';
		$item->border['type'] = $type;
		$item->border['size'] = $size;
		$item->border['color'] = $color;
		$this->assertEquals($valid, $item->isValid());
	}

}
