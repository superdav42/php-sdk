<?php namespace Newera\PlatformObjects;

use Newera\PlatformObjects\Exception\ItemException;

/**
 * Item Platform Object
 *
 * @copyright   (c) 2014 New Era Portfolio
 * @author      Shane Pearson <spearson@newerahd.com>
 */
class Item extends Object {

	const ERROR_EMPTY_SKU        = "Sku is required.";
	const ERROR_EMPTY_QUANTITY   = "Quantity is required.";

	public $id;
	public $sku;
	public $image;
	public $quantity;
	public $gpSku;
	public $itemSalePrice;
	public $salePrice;
	public $itemPrice;
	public $finalPrice;
	public $specs;
	public $notes;

	public function __construct($sku, $quantity = NULL) {
		if (is_string($sku)) {
			$sku = array('sku' => $sku, 'quantity' => $quantity);
		}
		else {
			$sku = (array) $sku;
		}

		foreach ($sku as $key => $val) {
			$this->{$key} = $val;
		}

		if ( ! empty($sku['image'])) {
			$this->image = new Image($sku['image']);
		}
		else {
			$this->image = new Image();
		}
	}

	public function getErrors() {
		$errors = array();

		if (empty($this->sku)) {
			$errors[] = Item::ERROR_EMPTY_SKU;
		}

		if (empty($this->quantity)) {
			$errors[] = Item::ERROR_EMPTY_QUANTITY;
		}

		$errors = array_merge($errors, $this->image->getErrors());

		return $errors;
	}

	public function toArray() {
		$arr = parent::toArray();
		$arr['properties'] = array(
			'sku' => $this->sku,
			'image' => $this->image->toArray()
		);
		return $arr;
	}

}

