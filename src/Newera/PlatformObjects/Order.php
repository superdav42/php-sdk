<?php namespace Newera\PlatformObjects;

use Newera\Api\Exception\ApiException;
use Newera\Api\Exception\ClientException;
use Newera\PlatformObjects\Exception\ItemException;

/**
 * Order Platform Object
 *
 * @copyright   (c) 2014 New Era Portfolio
 * @author      Shane Pearson <spearson@newerahd.com>
 */
class Order extends Object {

	public $id;
	public $status;
	public $purchaseDate;
	public $isTest;
	public $notes;
	public $totalPrice;
	public $totalItems;
	public $customer;
	public $shipping;
	public $billing;
	public $items;

	public function __construct($order = NULL) {
		if ( ! empty($order)) {
			foreach ($order as $key => $val) {
				$this->{$key} = $val;
			}

			if ( ! empty($this->customer)) {
				$this->customer = new Customer((array) $this->customer);
			}

			if ( ! empty($this->shipping)) {
				$this->shipping = new Address((array) $this->shipping);
			}

			if ( ! empty($this->billing)) {
				$this->billing = new Address((array) $this->billing);
			}

			$items = array();
			foreach ($this->items as $item) {
				$items[] = new Item($item);
			}
			$this->items = $items;
		}
	}

	public function getErrors() {
		$errors = array();

		if ( ! $this->customer instanceof Customer) {
			$errors[] = "Customer info must be type Customer.";
		}
		elseif ( ! $this->customer->isValid()) {
			$errors[] = "Customer info is not valid.";
		}

		if ( ! $this->shipping instanceof Address) {
			$errors[] = "Shipping address must be type Address.";
		}
		elseif ( ! $this->shipping->isValid()) {
			$errors[] = "Shipping info is not valid.";
		}

		if ( ! $this->billing instanceof Address) {
			$errors[] = "Billing address must be type Address.";
		}
		elseif ( ! $this->billing->isValid()) {
			$errors[] = "Billing info is not valid.";
		}

		foreach ($this->items as $item) {
			if ( ! $item instanceof Item) {
				$errors[] = "Item must be of type Item.";
			}
		}

		return $errors;
	}

	public function toArray() {
		$arr = parent::toArray();
		$arr['customer'] = $this->customer->toArray();
		$arr['shipping'] = $this->shipping->toArray();
		if ( ! empty($this->billing)) {
			$arr['billing'] = $this->billing->toArray();
		}
		foreach ($arr['items'] as $idx => $item) {
			$arr['items'][$idx] = $item->toArray();
		}
		return $arr;
	}

}
