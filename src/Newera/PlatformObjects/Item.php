<?php namespace Newera\PlatformObjects;

use Newera\PlatformObjects\Exception\ItemException;

/**
 * Item Platform Object
 *
 * @copyright   (c) 2014 New Era Portfolio
 * @author      Shane Pearson <spearson@newerahd.com>
 */
class Item extends Object {

	const ERROR_EMPTY_SKU        = "Sku is required.";
	const ERROR_EMPTY_QUANTITY   = "Quantity is required.";
	const ERROR_BORDER_TYPE      = "Border type must be mirror, blur or color.";
	const ERROR_BORDER_SIZE      = "Border size must be a positive integer.";
	const ERROR_BORDER_COLOR     = "Border color must be a valid CSS color.";

	/**
	 * Valid image types
	 * @var array
	 */
	public static $valid_border_types = array(
		'mirror', 'blur', 'color'
	);

	public $id;
	public $sku;
	public $image;
	public $quantity;
	public $gpSku;
	public $itemSalePrice;
	public $salePrice;
	public $itemPrice;
	public $finalPrice;
	public $specs;
	public $notes;
	public $border = array(
		'type'  => NULL,
		'size'  => NULL,
		'color' => NULL
	);

	public function __construct($sku, $quantity = NULL) {
		if (is_string($sku)) {
			$sku = array('sku' => $sku, 'quantity' => $quantity);
		}
		else {
			$sku = (array) $sku;
		}

		foreach ($sku as $key => $val) {
			$this->{$key} = $val;
		}

		if ( ! empty($sku['image'])) {
			$this->image = new Image($sku['image']);
		}
		else {
			//$this->image = new Image();
			unset($this->border);
		}
	}

	public function getErrors() {
		$errors = array();

		if (empty($this->sku)) {
			$errors[] = Item::ERROR_EMPTY_SKU;
		}

		if (empty($this->quantity)) {
			$errors[] = Item::ERROR_EMPTY_QUANTITY;
		}

		if ( ! empty($this->border['type']) &&
			 ! in_array($this->border['type'], Item::$valid_border_types))
		{
			$errors[] = Item::ERROR_BORDER_TYPE;
		}

		if ( ! is_null($this->border['size']) && $this->border['size'] < 0)
		{
			$errors[] = Item::ERROR_BORDER_SIZE;
		}

		if ( ! empty($this->border['color']) || $this->border['type'] == 'color')
		{
			if ( ! $this->validate_color($this->border['color']))
			{
				$errors[] = Item::ERROR_BORDER_COLOR;
			}
		}

		$errors = array_merge($errors, $this->image->getErrors());

		return $errors;
	}

	protected function validate_color($color)
	{
		if ($this->border['type'] == 'color' && empty($color))
		{
			return FALSE;
		}

		/** @TODO: validate CSS colors**/
		return TRUE;
	}

}
