<?php namespace Newera\PlatformObjects;

/**
 * Address Platform Object
 *
 * @copyright   (c) 2014 New Era Portfolio
 * @author      Shane Pearson <spearson@newerahd.com>
 */
class Address extends Object {
	public $name;
	public $company;
	public $address;
	public $city;
	public $state;
	public $zip;
	public $country;

	protected $required = array(
		'name', 'address', 'city', 'state', 'zip', 'country');

	public function __construct($init = array()) {
		foreach ($init as $k => $v) {
			$this->{$k} = $v;
		}
	}

	public function getErrors() {
		$errors = array();
		foreach ($this->required as $key) {
			if (empty($this->{$key})) {
				$errors[] = "Address $key is required.";
			}
		}
		return $errors;
	}
}
