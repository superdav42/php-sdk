<?php namespace Newera\PlatformObjects;

/**
 * Customer Platform Object
 *
 * @copyright   (c) 2014 New Era Portfolio
 * @author      Shane Pearson <spearson@newerahd.com>
 */
class Customer extends Address {
	public $phone;
	public $email;

	protected $required = array(
		'name', 'address', 'city', 'state', 'zip', 'country', 'phone', 'email');

	public function getErrors() {
		$errors = parent::getErrors();

		if ( ! filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
			$errors[] = "Email must be a valid email address.";
		}

		return $errors;
	}
}
