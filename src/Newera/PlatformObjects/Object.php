<?php namespace Newera\PlatformObjects;

/**
 * Platform Object
 *
 * @copyright   (c) 2014 New Era Portfolio
 * @author      Shane Pearson <spearson@newerahd.com>
 */
abstract class Object {
	abstract public function getErrors();

	public function isValid() {
		$errors = $this->getErrors();

		if ( ! empty($errors)) {
			return FALSE;
		}

		return TRUE;
	}

	public function toArray() {
		return (array) $this;
	}
}
