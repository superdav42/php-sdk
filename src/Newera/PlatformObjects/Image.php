<?php namespace Newera\PlatformObjects;

/**
 * Image Platform Object
 *
 * @copyright   (c) 2014 New Era Portfolio
 * @author      Shane Pearson <spearson@newerahd.com>
 */
class Image extends Object {

	const ERROR_EMPTY_IMAGE_TYPE = "Image type is required.";
	const ERROR_EMPTY_IMAGE_CODE = "Image code is required for New Era images.";
	const ERROR_EMPTY_IMAGE_URL  = "Image url is required for external images.";
	const ERROR_IMAGE_TYPE       = "Image type is not valid.";

	/**
	 * Valid image types
	 * @var array
	 */
	public static $valid_types = array(
		'newera', 'external'
	);

	/**
	 * Determine if an image type is valid
	 *
	 * @param  string $type    image type
	 * @return boolean
	 */
	public static function valid_type($type) {
		return in_array($type, Image::$valid_types);
	}

	public $type = 'newera';
	public $code;
	public $artist;
	public $url;

	public function __construct($image = array()) {
		foreach ($image as $key => $val) {
			$this->{$key} = $val;
		}
	}

	public function getErrors() {
		$errors = array();

		if (empty($this->type)) {
			$errors[] = Image::ERROR_EMPTY_IMAGE_TYPE;
		}

		if ( ! Image::valid_type($this->type)) {
			$errors[] = ERROR_IMAGE_TYPE;
		}

		switch($this->type) {
			case 'newera':
				if (empty($this->code)) {
					$errors[] = Image::ERROR_EMPTY_IMAGE_CODE;
				}
			break;
			case 'external':
				if (empty($this->url)) {
					$errors[] = Image::ERROR_EMPTY_IMAGE_URL;
				}
			break;
			default:
				$errors[] = Image::ERROR_IMAGE_TYPE;
		}

		return $errors;
	}

}
