<?php
/**
 * New Era PHP SDK
 *
 * @copyright   (c) 2013 New Era Portfolio
 */

namespace Newera\Api\Widget;

use Newera\Api\Exception\WidgetException;

/**
 * Configure and display the Javascript widget
 *
 * @since   1.0
 * @author  Shane Pearson <spearson@newerahd.com>
 */
class Widget {

	/**
	 * Widget url
	 * @var string
	 */
	protected $url;

	/**
	 * Widget key
	 * @var string
	 */
	protected $key;

	/**
	 * HTML container DOM id
	 * @var string
	 */
	protected $container;

	/**
	 * Widget script DOM id
	 * @var string
	 */
	protected $script;

	/**
	 * constructor
	 * @param array $config  Widget configuration
	 */
	public function __construct($config) {
		foreach (array('url', 'key', 'container', 'script') as $key) {
			if ( ! isset($config[$key])) {
				throw new WidgetException("Configuration error - {$key} not set.");
			}

			$this->{$key} = $config[$key];
		}
	}

	/**
	 * Render the widget HTML
	 *
	 * @return string
	 */
	public function render() {
		$html = '<script id="%s" type="text/javascript" src="%s"></script>'.PHP_EOL.
		        '<div id="%s"></div>';
		return sprintf($html, $this->script, $this->_url(), $this->container);
	}

	/**
	 * Generate the full widget url
	 *
	 * @return string
	 */
	protected function _url() {
		return $this->url.'?'.http_build_query(array('api_key' => $this->key));
	}

}
