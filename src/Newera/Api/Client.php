<?php
/**
 * New Era PHP SDK
 *
 * @copyright   (c) 2013 New Era Portfolio
 */

namespace Newera\Api;

use Newera\Api\Exception\ApiException;
use Newera\Api\Exception\ClientException;
use Newera\PlatformObjects\Exception\ItemException;
use Newera\PlatformObjects\Order;
use NewEra\PlatformObjects\Customer;
use NewEra\PlatformObjects\Address;
use NewEra\PlatformObjects\Item;

/**
 * API Client
 *
 * Sends HTTP requests to the image2art API servers.
 *
 * @copyright   (c) 2014 New Era Portfolio
 * @author      Shane Pearson <spearson@newerahd.com>
 */
class Client {

	const API_URL_TESTING    = 'http://api-testing.image2art.com/';
	const API_URL_STAGING    = 'http://api-stage.image2art.com/';
	const API_URL_PRODUCTION = 'https://api.image2art.com/';

	/** API environment to connect to */
	protected $environment;
	/** API url */
	protected $api_url;
	/** API username */
	protected $username;
	/** API secret */
	protected $secret;

	/** Hashing algorithm for signature generation */
	protected $hash_algo = 'sha256';

	/**
	 * constructor
	 * @param array $config
	 */
	public function __construct($config = array()) {
		foreach (array('environment', 'username', 'secret') as $field) {
			if (empty($config[$field])) {
				throw new ClientException("Configuration error: {$field} not set.");
			}
		}

		$this->environment = $config['environment'];
		$this->username    = $config['username'];
		$this->secret      = $config['secret'];

		switch($this->environment) {
			case 'testing':
				$this->api_url = Client::API_URL_TESTING;
			break;
			case 'staging':
				$this->api_url = Client::API_URL_STAGING;
			break;
			case 'production':
			default:
				$this->api_url = Client::API_URL_PRODUCTION;
		}

		if ( ! empty($config['hash_algo'])) {
			$this->hash_algo = $config['hash_algo'];
		}
	}

	/**
	 * Overload load calls to get(), post(), put() and delete()
	 *
	 * @param  string $method HTTP method
	 * @param  array $args    arguments to pass to _request
	 * @return mixed
	 */
	public function __call($method, $args) {
		if (in_array(strtoupper($method), Request::$allowed_methods)) {
			array_unshift($args, $method);
			return call_user_func_array(array($this, '_request'), $args);
		}
		return parent::__call($method, $args);
	}

	/**
	 * Send a POST request to create an order
	 *
	 * @param  Order            $order     order object
	 * @param  array            $extra     extra parameters to pass to order creation
	 * @return int                         the newly created order id
	 */
	public function createOrder(Order $order, $extra = array()) {
		foreach ($extra as $key => $val) {
			$order->{$key} = $val;
		}

		if ( ! $response = $this->post('orders', $order)) {
			return NULL;
		}

		return $response->order->id;
	}

	/**
	 * Send a GET request to retrieve order information
	 *
	 * @param  integer $limit  number of rows to return
	 * @param  integer $offset starting position
	 * @return array           order data
	 */
	public function getOrders($limit = 20, $offset = 0) {
		$response = $this->get('orders', array(
			'start' => $offset,
			'rows'  => $limit
		));

		return $response->orders;
	}

	/**
	 * Send a GET request to retrieve information for a single order.
	 *
	 * @param  int $id                 order id
	 * @return Newera\Orders\Order     order details
	 */
	public function getOrder($id) {
		if ( ! $response = $this->get("orders/$id")) {
			return NULL;
		}

		$order = new Order($response->order);

		return $order;
	}

	/**
	 * Get the status of an order
	 * @param  int $id     Order ID
	 * @return string     Order status
	 */
	public function getOrderStatus($id) {
		if ( ! $order = $this->getOrder($id)) {
			return NULL;
		}

		return $order->status;
	}

	/**
	 * Send a GET request to ping the API. This is only available in testing
	 * and staging environments.
	 *
	 * @return boolean
	 */
	public function ping() {
		if ( ! in_array($this->environment, array('testing', 'staging'))) {
			throw new ClientException('Method "ping" is only allowed in testing and staging environments.');
		}

		if ($response = $this->get('ping')) {
			return ($response->pong == 'ping');
		}

		return FALSE;
	}

	/**
	 * Sign a string using the user's secret key
	 *
	 * @param  string $uri  API request path
	 * @param  array  $data data being send with the request to be signed
	 * @return string       HMAC-SHA256 hash
	 */
	public function signRequest($uri, $data = array()) {
		// ignore username and signature if provided
		if (isset($data['username'])) {
			unset($data['username']);
		}
		if (isset($data['signature'])) {
			unset($data['signature']);
		}

		$data['username'] = $this->username;

		$req = $uri.json_encode($data);

		return hash_hmac($this->hash_algo, $req, $this->secret);
	}

	/**
	 * Execute an API request
	 * @param  string $method HTTP method
	 * @param  string $uri    request URI
	 * @param  array  $data   post fields
	 * @return Response       Response from API
	 */
	protected function _request($method = 'GET', $uri = NULL, $data = array()) {
		$method = strtoupper($method);
		$url    = $this->api_url.$uri;

		if ( ! is_array($data)) {
			$data = $data->toArray();
		}

		$data['username']  = $this->username;
		$data['signature'] = $this->signRequest($uri, $data);

		$request  = new Request($url, $data, $method);
		$response = $request->execute();
		$response = $response->body();

		if (isset($response->errorCode)) {
            
			throw new ApiException($response->error->long_message, $response->errorCode);
		}

		return $response;
	}
}
