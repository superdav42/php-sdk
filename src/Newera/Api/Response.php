<?php namespace Newera\Api;

class Response {

	protected $headers = array();
	protected $body;
	protected $code;
	protected $status;

	public function __construct($ch) {
		$raw = curl_exec($ch);
		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);

		$headers = trim(substr($raw, 0, $header_size));
		$headers = explode("\n", $headers);
		foreach ($headers as $key => $r) {
			if (stripos($r, 'HTTP/1.') !== FALSE) {
				list(,$this->code, $this->status) = explode(' ', $r);
			}
			else {
				list($headername, $headervalue) = explode(":", $r);
				$this->headers[$headername] = trim($headervalue);
			}
		}

		$this->body = substr($raw, $header_size);
	}

	public function body($body = NULL) {
		if (empty($body)) {
			return json_decode($this->body);
		}
		$this->body = is_array($body) ? json_encode($body) : $body;
	}

	public function code($code = NULL) {
		if (empty($code)) {
			return $this->code;
		}
		$this->code = $code;
	}

	public function header($header) {
		if (isset($this->headers[$header])) {
			return $this->headers[$header];
		}
		return NULL;
	}

	public function headers() {
		return $this->headers;
	}
}
