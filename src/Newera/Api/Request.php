<?php
/**
 * New Era PHP SDK
 *
 * @copyright   (c) 2013 New Era Portfolio
 */

namespace Newera\Api;

use Newera\Api\Exception\ApiException;

/**
 * Send HTTP Requests to the image2art.com API servers.
 *
 * @since   1.0
 * @author  Shane Pearson <spearson@newerahd.com>
 */
class Request {
	const GET    = 'GET';
	const POST   = 'POST';
	const PUT    = 'PUT';
	const DELETE = 'DELETE';

	/**
	 * Allowed HTTP methods
	 * @var array
	 */
	public static $allowed_methods = array(
		Request::GET,
		Request::POST,
		Request::PUT,
		Request::DELETE
	);

	/**
	 * HTTP method
	 * @var string
	 */
	protected $method = Request::GET;

	/**
	 * Request url
	 * @var string
	 */
	protected $url    = '';

	/**
	 * POST data
	 * @var array
	 */
	protected $data   = array();

	/**
	 * cURL options
	 * @var array
	 */
	protected $curl_options = array();

	/**
	 * constructor
	 * @param string $url    Request url
	 * @param array $data   POST data
	 * @param string $method HTTP method
	 */
	public function __construct($url = '', $data = NULL, $method = NULL) {
		$this->url    = $url;

		if ( ! empty($data)) {
			$this->data = (array) $data;
		}

		if ( ! empty($method)) {
			$this->setMethod($method);
		}

		$this->curl_options = array(
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_HEADER         => TRUE,
			CURLOPT_HTTPHEADER     => array(
				'Content-Type: application/json',
				'Accept: application/json',
				'Expect:'
			)
		);
	}

	/**
	 * Execute the HTTP request
	 * @return Response
	 */
	public function execute() {
		$options = $this->curl_options + array(
			CURLOPT_CUSTOMREQUEST  => $this->method,
			CURLOPT_POSTFIELDS     => json_encode($this->data)
		);

		$ch = curl_init($this->url);
		curl_setopt_array($ch, $options);
		$response = new Response($ch);
		curl_close($ch);

		return $response;
	}

	/**
	 * Set the HTTP method
	 *
	 * @param string $method HTTP method
	 */
	public function setMethod($method) {
		if ( ! in_array($method, Request::$allowed_methods)) {
			throw new ApiException("Method not allowed: {$method}");
		}

		$this->method = $method;
		return $this;
	}

	/**
	 * Set the request url
	 *
	 * @param string $url  Request url
	 */
	public function setUrl($url) {
		$this->url = $url;
		return $this;
	}

	/**
	 * Set POST data
	 * @param array $data POST data
	 */
	public function setData($data) {
		$this->data = (array) $data;
		return $this;
	}
}
