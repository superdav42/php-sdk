<?php
/**
 * New Era PHP SDK
 *
 * @copyright   (c) 2013 New Era Portfolio
 */

namespace Newera\Api\Exception;

/**
 * ClientException Class
 */
class ClientException extends \Exception {}
