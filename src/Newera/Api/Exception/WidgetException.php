<?php
/**
 * New Era PHP SDK
 *
 * @copyright   (c) 2013 New Era Portfolio
 */

namespace Newera\Api\Exception;

/**
 * Widget Exception Class
 *
 * {@inheritDoc}
 */
class WidgetException extends \Exception {}
