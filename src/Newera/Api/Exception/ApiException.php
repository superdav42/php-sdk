<?php
/**
 * New Era PHP SDK
 *
 * @copyright   (c) 2013 New Era Portfolio
 */

namespace Newera\Api\Exception;

/**
 * ApiException Class
 */
class ApiException extends \Exception {}
