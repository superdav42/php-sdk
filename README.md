# New Era PHP SDK

The New Era PHP SDK is a PHP interface to the
[image2art REST API](http://docs.image2art.com).

## Installation

The package is available on [packagist.org](https://packagist.org/packages/newera/php-sdk) so installation is simple
using [Composer](http://getcomposer.org). Just add the following dependency to
your composer.json file and run `composer update`.

	"require": {
		"newera/php-sdk": "dev-master"
	}

## Setting up the Client

The SDK includes a client to connect to the REST API. The client supports all of
the environments from api.image2art.com. To set the environment, simply pass the
environment name to the client constructor.

The allowed environments are:

- testing
- staging
- production

The example below shows how to initialize the client with the testing user
credentials.

	use Newera\Api\Client;

	$this->client = new Client(array(
		'environment' => 'testing',
		'username'    => 'testing',
		'secret'      => '$2y$08$tEwCIc9DxDNKRvzHhfRRlOHxDbu.MyIk5EnzEmZVEJ/GeuHXa.tki'
	));

## Platform Objects

Support for **Platform Objects** is also included. **Orders**, **Items**,
**Addresses** and **Customers** can all be created and validated with the SDK.

For more on **Platform Objects** please refer to the
[REST API documentation](http://docs.image2art.com/#platform-objects).

## Using the Client

The client comes with shortcut methods to the REST API. These methods are
outlined below. More detailed API documentation, including class descriptions
for **Platform Objects**, is generated with **phpdoc** and
is available in the `docs` folder.

### ping

Sends a GET request to /ping and return TRUE if the server is up.

#### example

	$server_up = $this->client->ping();
	// returns TRUE

### createOrder

Sends a POST request to /orders to create an **Order**. This method takes a
**Platform Object** of type **Order** as the only parameter.

#### example

	use Newera\PlatformObjects\Address;
	use Newera\PlatformObjects\Customer;
	use Newera\PlatformObjects\Item;
	use Newera\PlatformObjects\Order;

	$order = new Order();

	// set the customer information
	$order->customer = new Customer();

	// set shipping and billing
	$order->shipping = new Address();
	$order->billing  = new Address();

	// add items
	$order->items = array();
	$order->items[] = new Item();

	// create the order
	$order_id = $this->client->createOrder($order);

### getOrders

Sends a GET request to /orders to retrieve all orders from the system

#### example

	$orders = $this->client->getOrders(5);
	echo count($orders); // 5

### getOrder

Sends a GET request to /orders/:id to retrieve a specified order.

#### example

	// return an Order object
	$order = $this->client->getOrder(10001);

### getOrderStatus

Shortcut method to retrieve the status of a specified order.

	echo $this->client->getOrderStatus(10001);
	// prints "IN TRANSIT"

## Setup for Development

To set up the project for development, clone the repository and run `composer
install` to generate the class loader for testing.

	$ git clone git@bitbucket.org:newera/php-sdk.git
	$ cd php-sdk
	$ composer install

### Running Unit tests

Now that Composer has resolved all dependencies, you can run `phpunit` from the
project root folder. Tests can be found in the `tests` folder. Any new tests
you add here will be run as part of the test suite.

	$ phpunit
